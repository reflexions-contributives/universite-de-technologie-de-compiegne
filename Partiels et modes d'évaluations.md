Dans cet écrit, je m'interroge sur la légitimité des médians, finaux et partiels à l'UTC (et plus largement en vrai, mais on parle avat tout de son quotidien).
Attention ce texte à une visée constructive, donc toute critique ciblée non constructive est mal venue.

Une heure trente à gribouiller le plus rapidement possible pour montrer que  l'on sait faire.
Une heure trente pour rendre un travail pas des plus qualitatifs.
Une heure trente c'est vraiment un temps très court pour travailler sur un sujet. C'est vrai, quand on se projette dans le milieu professionnel, on se rend compte qu'on ne fera pas -ou très rarement- face à ce genre de contraintes temporelles si courtes.
En réalité, une organisation plus saine se rapproche de celle des projets (individuels ou collectifs) : on se pose différentes deadlines, on fait notre rétro-planning et on s'organise pour avoir le temps de travailler sur notre sujet.
C'est en cela que je trouve que les partiels se détachent de la réalité.

Ce n'est pas pour autant que je pense qu'il faille supprimer ce mode d'évaluation. Non, je suis pour une diversité des modes d'examens, mais lorsque la grande majorité des UVs sont évalué de 70% à 100% par des examens je trouve ce choix pédagogique problématique...
En pratique, on nous forme ainsi à répondre à des questions types dans les annales. On nous apprends à automatiser des réponses pour être efficace le jour J pendant une heure trente. Est-ce que ce mode d'évaluation permet de garder à long terme les connaissances et techniques transmises ? Je n'en suis pas convaincu...

L'entraînement par les annales, le "bachotage", n'est pas une mauvaise chose initialement. C'est toujours intéressant de tenter de comprendre par la pratique. C'est plutôt le format applicatifs, automatisé sans chercher le sens, la compréhension des phénomènes qui me dérange ; on apprends à connaître les techniques de réponses.

Je suis conscient de l'ancrage de ce mode d'évaluation dans le système éducatif.
Je pense qu'il faudrait davantage développer et laisser de la place aux autres modes d'évaluation :
EXISTANT A DEVELOPPER OU NON EXISTANT
- Projets en tout genre
- Ateliers-Projets (GU, IM) : Se rassembler à plusieurs pour répondre à un projet d'un industriel (projection professionnelle très intéressantes)
- Exposées, Mémoire, Rapport : Recherche personnelle ou groupées pour transmettre des connaissances précises (apprendre par la transmission)
- TD où on évalue les compétences de l'UV progressivement et plsuieurs fois
- ... Des idées ?



La question qu'il faut se poser : COMMENT EVALUER LA COMPREHENSION DES CONNAISSANCES et TECHNIQUES ?
--> Je pense que le format partiel est bii


